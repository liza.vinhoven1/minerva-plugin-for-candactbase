# MINERVA Plugin for CandActBase

This MINERVA Plugin will connect the compounds of the CandActBase to a given MINERVA instance and their respective entities via interaction databases ChEMBL (https://www.ebi.ac.uk/chembl/) and CTD (http://ctdbase.org/).
This Plugin was built upon the MINERVA starter-kit for Plugin building (https://git-r3lab.uni.lu/minerva/plugins/starter-kit/-/tree/master/).

To see a live demo of the plugin go to: https://cf-map.uni-goettingen.de/minerva/index.xhtml?id=WholeCell

## Building and Editing
The Plugin can be edited in the [src/js/index.js](src/js/index.js) and afterwards packed with the [npm](https://nodejs.org/en/download/) package manager.
- Download the [npm](https://nodejs.org/en/download/) package manager
- run `npm install`
- build your plugin.js with `npm run build`
- you can then link the built plugin.js in your MINERVA instance to load the plugin (make sure to upload the plugin.js where the MINERVA instance can access it, e.g on Github)

## Use this Plugin
To access this plugin in your MINERVA instance copy this link into the add plugin mask "https://raw.githubusercontent.com/mvoskamp/plugin_test/main/plugin.js"

Moreover, you need to place the "targetdata" folder where your browser can access it. You might have to alter the paths from the CandActBase plugin and repackage the plugin to make the folder accessible.

For more information visit the [starter-kit](https://git-r3lab.uni.lu/minerva/plugins/starter-kit/-/tree/master/) and the MINERVA [help pages](https://minerva.pages.uni.lu/doc/admin_manual/v15.0/index/#plugins)

## Use the plugin with other targets and disease maps
If you want to use this plugin with different data, you need to adjust these settings and create following data:
- `index.js`/`plugin.js`: alter the ajax calls to obtain informations for searched compounds (to see which JSON structure is needed, have a look at the output of the respective CandActBase response or the files in the targetdata folder). If the response format is different, the code need to be adjusted accordingly
- `/targetdata/compoundlistwithentities.json`: This file contains a list of all the targets for all the compounds identified by the CandActBase ID. 
- `/targetdata/entitylistwithcompoundswithCTD.json`: This JSON consits of the name of every entity of the loaded disease-map. For each enitityname there is a list with IDs for compounds from the CandActBase.

To create both latter JSON files you would need to get the enititynames from your disease map, look up interaction partners in suitable databases (e.g. CTD, ChEMBL) and join those found targets with your compound database.
