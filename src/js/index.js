// written by Liza Vinhoven (liza.vinhoven@med.uni-goettingen.de) & Malte Voskamp (malte.voskamp@stud.uni-goettingen.de)
// build upon the base of the minerva starter-kit (https://git-r3lab.uni.lu/minerva/plugins/starter-kit)

require('../css/styles.css');

const pluginName = 'CandActBase';
const pluginVersion = '1.0.4';

const containerName = pluginName + '-container';

const minervaProxyServer = 'https://minerva-dev.lcsb.uni.lu/minerva-proxy/';

const globals = {
    selected: [],
    allBioEntities: [],
    pickedRandomly: undefined,
    alltargets: undefined,
    entitytocompounds: [],
    compoundselected: -1
};

let $ = window.$;
if ($ === undefined && minerva.$ !== undefined) {
    $ = minerva.$;
}

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;
let minervaVersion;

const register = function(_minerva) {

    //console.log('registering ' + pluginName + ' plugin');

    _minerva.pluginData.setGlobalParam("test","yupi");

    $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);
    pluginContainerId = pluginContainer.attr('id');
    if (!pluginContainerId) {
        pluginContainerId = pluginContainer.parent().attr('id');
    }


    //console.log('minerva object ', minervaProxy);
    //console.log('project id: ', minervaProxy.project.data.getProjectId());
    //console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    return minerva.ServerConnector.getConfiguration().then( function(conf) {
        minervaVersion = parseFloat(conf.getVersion().split('.').slice(0, 2).join('.'));
        //console.log('minerva version: ', minervaVersion);
        initPlugin();
    });
};

const unregister = function () {
    //console.log('u,nregistering ' + pluginName + ' plugin');

    unregisterListeners();
    return deHighlightAll();
};

const getName = function() {
    return pluginName;
};

const getVersion = function() {
    return pluginVersion;
};


minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion,
        minWidth: 400,
        defaultWidth: 500
    }
});

function initPlugin () {
    registerListeners();
    initMainPageStructure();
    pluginContainer.find(`.${containerName}`).data("minervaProxy", minervaProxy);
    checkentities();
}

function registerListeners(){
    minervaProxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: searchListener
    });
}

function unregisterListeners() {
    minervaProxy.project.map.removeAllListeners();
}

function deHighlightAll(){

    //console.log("deHighlightAll")

     return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// checks whether entites have been loaded into the plugin
function checkentities() {
	if (globals.allBioEntities.length == 0) {
        minervaProxy.project.data.getAllBioEntities().then(function(bioEntities) {
            globals.allBioEntities = bioEntities;
            loaddata()
        });
    }
}

// loads target data from JSON files into the minerva instance
function loaddata() {
    $.ajax({
        url: `../targetdata/entitylistwithcompoundswithCTD.json`,
        success: function(result) {
            globals.entitytocompounds = result
         }
    }) 

    $.ajax({
        url: `../targetdata/compoundlistwithentities.json`,
        success: function(result) {
            globals.alltargets = result
        }
    }) 
}

// creates the html for the plugin structure with inline css
function initMainPageStructure(){
    const container = $(`<div class=${containerName} ></div>`).appendTo(pluginContainer);
    container.append(`
         <div class="container">
            <div class ="row" style ="background: #79b94c; color:white;">
                
                <div class ="col-md-6">
                    <h3 style=" padding-top:9%"> CandActCFTR Plugin</h3>
                </div> 
                <div class ="col-md-6" >
                    <img src='../assets/grails-cupsonly-logo-white.svg' alt="CandActBaseLogo" style= "float:right;width:60%">

                </div> 
            </div>
        </div>
         
    `)
    container.append('<hr>');
    container.append(`
        <div class="panel panel-default card panel-events mb-2">
            <div class="panel-heading card-header" style="background-color:#4D8618; color:white; text-align: center">Compound Search in CandActBase</div>
            <div>
                <form id = "candactsearchform">
                    <input type="text" placeholder="Search for Compound..." id="inputcandactsearch" style="width: 80%; float:left" class="form-control">
                    <button id="candactsearchbutton" type="button" class="btn-candactsearch btn btn-primary btn-default btn-block" style="width:20%; float:right"> Search</button>
                </form>
            </div>
            <div class="panel-body card-body" id="candactcompoundresult">

            </div>
        </div>
    `);
    container.append('<hr>');
    container.append(`
        <div class="panel panel-default card panel-events mb-2">
            <div class="panel-heading card-header" style="background-color:#4D8618; color:white;  text-align: center">Select one Element in Graph</div>
            <div class="panel-body card-body" id = "targetsearchresult">                
            </div>
        </div>
    `);
    container.append('<hr>');
    container.append(`
         <div class ="panel panel-default card panel-events" style="background-color:#4D8618; color:white; text-align: center; margin-bottom: 24%;">
           <form>
            <p style="width: 80%; float:left; padding-top:2%;">Show all targets from all Compounds in CandActBase</p>

         
             <button type="button" class="btn-showalltarget btn btn-primary btn-default"  style="width:20%; height:100%"> Show all </button>
             </form>
         </div>
    `);
    container.append(`
            <div class="panel panel-default card panel-events mb-2" style ="position:absolute;bottom:0px; width:100%">
            <div id="selectedcompounds" class="panel-heading card-header" style="background-color:#79b94c; color:white;  text-align: center; width:100%">Select one or more Compounds

            </div>
             <button id ="intersectionList" type="button" class="btn btn-primary btn-default" data-toggle="modal" data-target="#exampleModal" style="display:none"> Show list of Targets! </button>
      
            </div>
        
        `)

    container.append(`
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">List of Targets!</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id ="modalbodyheader">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
    </div>`)
    

    container.find('.btn-candactsearch').on('click',() => searchCandact() );
    container.find('.btn-showalltarget').on('click',() => highlightalltargetsfromcompounds() );
    
    var input = document.getElementById("candactsearchform");

    input.addEventListener("keydown", function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            document.getElementById("candactsearchbutton").click();
        }
    }); 
}

// gets calles when an entity is clicked
// loads all compounds that have the entity as a target and then calls the candactbase for compound information
function searchListener(entites) {
   
    deHighlightAll().then(function() {
        var selectedcompounds = document.getElementById("selectedcompounds")
        selectedcompounds.innerHTML = `Select one or more Compounds`
        globals.compoundselected = -1
        var intersectionButton = document.getElementById("intersectionList")
        intersectionButton.style.display ="none"

        var div = document.getElementById('targetsearchresult')

        globals.selected = entites[0];

        let targetstr = '';
        let compoundofentitystring = '';
        let searchstring = ''

        var topdiv = document.createElement("div")
        var bottomdiv = document.createElement("div")
        var compoundofentity = []

        div.innerHTML =""
        bottomdiv.innerHTML = ""
        topdiv.innerHTML = "...loading"

        if (globals.selected.length > 0) {
            globals.selected.forEach(function(entity) {
                if(entity.constructor.name === 'Alias') {
                    targetstr += `<div> <b>${entity.getName()}</b> - ${entity.getElementId()}</div>`
                    compoundofentity = globals.entitytocompounds[entity.getName()]
                }

                div.innerHTML = targetstr + "</b> loading <b>" + compoundofentity.length +"</b> Compounds..."
                if(compoundofentity.length > 0) {
                    
                    div.innerHTML = ""
                    compoundofentity.forEach(function(compoundID) {

                        $.ajax({
                            url: 'https://candactcftr.ams.med.uni-goettingen.de/Compound/cycleCompounds.json?id='+compoundID,
                            success: function(result) {
                                var compound = result['currentCompound']
                                var synonyms = result['currentPubChemSynonym']
                                var synonymsString = ""
                                synonyms.forEach(function(synonymentry) {
                                    synonymsString += synonymentry["synonym"]
                                })

                                topdiv.innerHTML = `<h3>Found <b>${compoundofentity.length}</b> Compounds</h3> </br>
                                            <p> for clicked Target: <b> ${targetstr} </b> </p>`
                                var cyclediv = document.createElement("div")

                                var compounddiv = document.createElement("div")
                                var synonymdiv = document.createElement("div")

                                var htmltext = '<hr>'
                                htmltext += `<b>ID:</b> <a href="https://candactcftr.ams.med.uni-goettingen.de/Compound/cycleCompoundsKekuleStyle/${compound['id']}">${compound['id']}</a></br>
                                            <b>InChIKey:</b> ${compound['inChIKey']}</br> 
                                            <b>SMILES:</b> ${compound['smiles']} `

                                htmltext += `<div id="PubChemPane">
                                            <br/>
                                            <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchikey/${compound['inChIKey']}/PNG" alt="chemical compound" style="width:304px;height:228px;">
                                            <br/>
                                            <p></br></p>
                                        </div>`


                                var firstchar = synonymsString.substring(0,60)
                                var firstchardiv = document.createElement("span")
                                firstchardiv.innerHTML = firstchar

                                var readmore = document.createElement("a")
                                readmore.innerHTML = "...more"
                                readmore.style.textDecoration = "underline"; 
                                readmore.style.color = "blue"

                                var wholesyndiv = document.createElement("span")
                                wholesyndiv.innerHTML = synonymsString
                                wholesyndiv.style.display = "none"

                                var readless = document.createElement("a")
                                readless.innerHTML = "View less"
                                readless.style.display = "none"
                                readless.style.textDecoration = "underline"; 
                                readless.style.color = "blue"

                                readmore.onclick = function() {
                                    firstchardiv.style.display = "none"
                                    readmore.style.display = "none"
                                    wholesyndiv.style.display = "block"
                                    readless.style.display ="block"
                                }

                                
                                readless.onclick = function() {
                                    firstchardiv.style.display = "block"
                                    readless.style.display = "none"
                                    wholesyndiv.style.display = "none"
                                    readmore.style.display ="block"
                                }

                                synonymdiv.appendChild(firstchardiv)
                                synonymdiv.appendChild(readmore)
                                synonymdiv.appendChild(wholesyndiv)
                                synonymdiv.appendChild(readless)

                                compounddiv.innerHTML = htmltext

                                cyclediv.appendChild(compounddiv)
                                cyclediv.appendChild(synonymdiv)

                                var targetbutton = document.createElement("button")
                                targetbutton.innerHTML = `SHOW TARGETS FROM ID: ${compound['id']} (${globals.alltargets[compound['id']].length})`
                                targetbutton.classList.add("btn")
                                targetbutton.classList.add("btn-primary")
                                targetbutton.classList.add("btn-default")

                                targetbutton.onclick = function() {
                                    highlightalltargetsfromcompounds(compound['id'])
                                }

                                var comparebutton = document.createElement("button")
                                comparebutton.innerHTML = 'COMPARE Compound'
                                comparebutton.classList.add("btn")
                                comparebutton.classList.add("btn-primary")
                                comparebutton.classList.add("btn-default")
                                comparebutton.style.marginLeft ="2%"

                                cyclediv.appendChild(targetbutton)
                                cyclediv.appendChild(comparebutton)

                                comparebutton.onclick = function() {
                                    if(globals.compoundselected > 0) {
                                        highlightalltargetsfromcompounds(compound['id'],true)
                                    }else {
                                        highlightalltargetsfromcompounds(compound['id'])
                                    }
                                }
                                bottomdiv.appendChild(cyclediv)
                                div.appendChild(topdiv)
                                div.appendChild(bottomdiv)
                            },error: function(error) {
                                topdiv.innerHTML = "there was an error with query</br>"
                                topdiv.innerHTML += error
                                div.appendChild(topdiv)
                                div.appendChild(bottomdiv)
                                console.log(error)
                            } 
                        })
                    })
                    div.appendChild(topdiv)
                    div.appendChild(bottomdiv)
                    
                } else {
                    if(entity.constructor.name === 'Alias') {
                        compoundofentitystring += "No Compound for<b> " + entity.getName() + "</b> found."
                    } else {
                        compoundofentitystring += "Cannot find Compound for entities from this "
                    }
                    div.innerHTML = ""
                    div.innerHTML = compoundofentitystring
                    topdiv.innerHTML = ""
                    div.appendChild(topdiv)
                    div.appendChild(bottomdiv)
                }
            })
        }
    })
}

// listens to input in the compound search mask
// calls the candactbase to search for similar compound names
function searchCandact() {

    var searchquery = document.getElementById('inputcandactsearch').value
    var foundtargets = false
    var resultdiv = document.getElementById("candactcompoundresult")
    var topdiv = document.createElement("div")
    var bottomdiv = document.createElement("div")

    resultdiv.innerHTML =""
    bottomdiv.innerHTML = ""
    topdiv.innerHTML = "loading..."
    if(searchquery == "") {
        alert("insert searchquery; cannot search empty string")
        return
    }
    
    $.ajax({
        url: 'https://candactcftr.ams.med.uni-goettingen.de/Compound/searchCompoundsBySynonym.json?Synonym="'+searchquery+'"',
        success: function(result) {
            var compounds = result['currentCompounds']
            var nrofHits = result["nrOfHits"]
            if (nrofHits == 0) {
                topdiv.innerHTML = `No Compound with Synonym <b> ${searchquery} </b> was found </br>
                            maybe try something else`
            } else {
                topdiv.innerHTML = `<h3>Search result for: <b>${searchquery}</b></h3> </br>
                            <p> found <b>${nrofHits}</b> results </p>
                            `
                compounds.forEach(function(entry) { 
                    var cyclediv = document.createElement("div")

                    if(entry['id'] in globals.alltargets && globals.alltargets[entry['id']].length > 0) {
                        console.log(entry['id'] + "has " + globals.alltargets[entry['id']].length +" in the graph")
                        foundtargets = true

                        var compounddiv = document.createElement("div")
                        var synonymdiv = document.createElement("div")
                        synonymdiv.id = "synonym" + entry['id']

                        var htmltext = '<hr>'
                        htmltext += `<b>ID:</b> <a href="https://candactcftr.ams.med.uni-goettingen.de/Compound/cycleCompoundsKekuleStyle/${entry['id']}">${entry['id']}</a></br>
                                    <b>InChIKey:</b> ${entry['inChIKey']}</br> 
                                    <b>SMILES:</b> ${entry['smiles']} `

                        htmltext += `<div id="PubChemPane">
                                    <br/>
                                    <img src="https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchikey/${entry['inChIKey']}/PNG" alt="chemical compound" style="width:304px;height:228px;">
                                    <br/>
                                    <p></br></p>
                                </div>`

                        compounddiv.innerHTML = htmltext

                        cyclediv.appendChild(compounddiv)
                        cyclediv.appendChild(synonymdiv)

                        var targetbutton = document.createElement("button")
                        targetbutton.innerHTML = `SHOW TARGETS FROM ID: ${entry['id']} (${globals.alltargets[entry['id']].length})`
                        targetbutton.classList.add("btn")
                        targetbutton.classList.add("btn-primary")
                        targetbutton.classList.add("btn-default")

                        targetbutton.onclick = function() {
                            highlightalltargetsfromcompounds(entry['id'])
                        }

                        var comparebutton = document.createElement("button")
                        comparebutton.innerHTML = 'COMPARE Compound'
                        comparebutton.classList.add("btn")
                        comparebutton.classList.add("btn-primary")
                        comparebutton.classList.add("btn-default")
                        comparebutton.style.marginLeft ="2%"

                        cyclediv.appendChild(targetbutton)
                        cyclediv.appendChild(comparebutton)

                        comparebutton.onclick = function() {
                            if(globals.compoundselected > 0) {
                                highlightalltargetsfromcompounds(entry['id'],true)
                            }else {
                                highlightalltargetsfromcompounds(entry['id'])
                            }
                        }
                    }
                    bottomdiv.appendChild(cyclediv)
                })
                if(foundtargets == false) {
                    topdiv.innerHTML = `No Targets for <b> ${searchquery} </b> were found </br>
                    maybe try something else`
                } else {
                    compounds.forEach(function(entry) {

                        var synonymdivID = "synonym" + entry['id']
                        var synonymdiv = document.getElementById(synonymdivID)
                        $.ajax({
                            url: 'https://candactcftr.ams.med.uni-goettingen.de/Compound/cycleCompounds.json?id='+entry['id'],
                            success: function(result) {
                                var synonyms = result['currentPubChemSynonym']
                                var synonymsString = ""
                                synonyms.forEach(function(synonymentry) {
                                    synonymsString += synonymentry["synonym"]
                                })
                                
                                var firstchar = synonymsString.substring(0,60)
                                var firstchardiv = document.createElement("span")
                                firstchardiv.innerHTML = firstchar
                
                                var readmore = document.createElement("a")
                                readmore.innerHTML = "...more"
                
                                readmore.style.textDecoration = "underline"; 
                                readmore.style.color = "blue"
                                
                                var wholesyndiv = document.createElement("span")
                                wholesyndiv.innerHTML = synonymsString
                                wholesyndiv.style.display = "none"
                
                                var readless = document.createElement("a")
                                readless.innerHTML = "View less"
                                readless.style.display = "none"
                                readless.style.textDecoration = "underline"; 
                                readless.style.color = "blue"
                
                                readmore.onclick = function() {
                                    firstchardiv.style.display = "none"
                                    readmore.style.display = "none"
                                    wholesyndiv.style.display = "block"
                                    readless.style.display ="block"
                                }
                                
                                readless.onclick = function() {
                                    firstchardiv.style.display = "block"
                                    readless.style.display = "none"
                                    wholesyndiv.style.display = "none"
                                    readmore.style.display ="block"
                                }
                
                                synonymdiv.appendChild(firstchardiv)
                                synonymdiv.appendChild(readmore)
                                synonymdiv.appendChild(wholesyndiv)
                                synonymdiv.appendChild(readless)
                            }
                        })
                    })
                }
            }
        },error: function(error) {
            topdiv.innerHTML = "there was an errot with query:" + searchquery + "</br>"
            topdiv.innerHTML += error
            console.log(error)
        } 
    })
    resultdiv.appendChild(topdiv)
    resultdiv.appendChild(bottomdiv)
}

// highlights all targets from compounds
function highlightalltargetsfromcompounds(id = -1, compare = false) {

    var all = globals.entitytocompounds
    var entityarray = []
    var selectedcompounds = document.getElementById("selectedcompounds")
    
    if(id < 0) {

        var entitynamelist = Object.keys(all)
        selectedcompounds.innerHTML = `Showing <b> ALL </b> Targets from all Compounds.`
        var intersectionButton = document.getElementById("intersectionList")
        var modalbodyheader = document.getElementById("modalbodyheader")
        modalbodyheader.innerHTML = `Showing <b> ALL </b>  Targets from all Compounds  <hr></br> `

        for(var entity of entitynamelist) {
            modalbodyheader.innerHTML += `<p> ${entity} </p>`
        }
        var intersectionButton = document.getElementById("intersectionList")
        intersectionButton.style.display ="block"
        globals.compoundselected = -1
    }else {
        var entitynamelist = globals.alltargets[id]
    }

    if(compare) {
        var prevCompoundentities = globals.alltargets[globals.compoundselected]
        var intersection = prevCompoundentities.filter(element => entitynamelist.includes(element))
        for(var entity of intersection) {
            if(!jQuery.isEmptyObject(all[entity])) {
                var entitiesingraph = checkgraphfortarget(entity) 
                entitiesingraph.forEach(function(entityingraph) {
                    entityarray.push(entityingraph)
                })
            }
        }

        var selectedcompounds = document.getElementById("selectedcompounds")
        selectedcompounds.innerHTML = `Showing ${intersection.length} matching Targets from Compound with ID: <b> ${id} & ${globals.compoundselected}`
        var modalbodyheader = document.getElementById("modalbodyheader")
        modalbodyheader.innerHTML = `Showing ${intersection.length} matching Targets from Compound with ID: <b> ${id} & ${globals.compoundselected} <hr></br> `

        for(var entity of intersection) {
            modalbodyheader.innerHTML += `<p> ${entity} </p>`
        }
        var intersectionButton = document.getElementById("intersectionList")
        intersectionButton.style.display ="block"
    } else {
        for(var entity of entitynamelist) {
            if(!jQuery.isEmptyObject(all[entity])) {
                var entitiesingraph = checkgraphfortarget(entity) 
                entitiesingraph.forEach(function(entityingraph) {
                    entityarray.push(entityingraph)
                })
            }
        }
        if(id > -1) {
            var selectedcompounds = document.getElementById("selectedcompounds")
            selectedcompounds.innerHTML = `Showing <b> ${globals.alltargets[id].length} </b> Targets from Compound with ID: <b> ${id} </b> </br> Select another Compound to see matching targets.`
            globals.compoundselected = id

            var intersectionButton = document.getElementById("intersectionList")

            var modalbodyheader = document.getElementById("modalbodyheader")
            modalbodyheader.innerHTML = `Showing ${globals.alltargets[id].length} matching Targets from Compound with ID: <b> ${id} <hr>  </br>`

            for(var entity of entitynamelist) {
                modalbodyheader.innerHTML += `<p> ${entity} </p>`
            }

            var intersectionButton = document.getElementById("intersectionList")
            intersectionButton.style.display ="block"
        }
    }
    selectentities(entityarray, compare)
}

// get targets from graph
function checkgraphfortarget(target) {
    var targetarray = globals.allBioEntities.filter(be => {
            return be.name == target
        })
    return targetarray
}

// highlight entities in the graph
function selectentities(entityarray, compare = false) {
    deHighlightAll().then(function() {
        if(globals.allBioEntities.length > 0) {
            const highlightDefs = [];
            entityarray.forEach(e => {
                highlightDefs.push({
                    element: {
                        id: e.id,
                        modelId: e.getModelId(),
                        type: "ALIAS"
                    },
                    type: "ICON"
                });
            });
            minervaProxy.project.map.showBioEntity(highlightDefs);
        }else {
            alert("no entities in graph found")
        }
    })    
}



